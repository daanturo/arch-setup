#!/usr/bin/env bash
source $(dirname $0)/env.sh

### podman

$_AUR_HELPER -S --noconfirm --needed podman

_registries=$(pcregrep -o1 '= "([^"]*?)/' /etc/containers/registries.conf.d/00-shortnames.conf | sort | uniq)

# https://wiki.archlinux.org/title/Podman#Container_DNS_will_not_be_enabled
$_AUR_HELPER -S --noconfirm --needed aardvark-dns

sudo touch /etc/subuid /etc/subgid
sudo chmod 644 /etc/subuid /etc/subgid

if ! grep -q -E "^$USER:" /etc/subuid; then
    set -x
    _existing_users=$(wc -l </etc/subuid)
    _min_uid=$(userdbctl | pcregrep -o1 'begin container users.*? ([0-9]+)')
    _beg_uid=$((_min_uid + 65536 * _existing_users))
    _end_uid=$((_min_uid + 65536 * (_existing_users + 1) - 1))
    sudo usermod --add-subuids $_beg_uid-$_end_uid --add-subgids $_beg_uid-$_end_uid $USER
    { set +x; } 2>/dev/null
fi

podman system migrate

systemctl --user enable --now podman.socket

# Stop outputting "Activate the web console with: systemctl enable --now cockpit.socket" when logining
# https://access.redhat.com/solutions/6954311
# https://www.itechlounge.net/2021/07/linux-how-to-disable-the-message-activate-the-web-console-with-on-rhel-8/
sudo ln -sfn /dev/null /etc/motd.d/cockpit
sudo ln -sfn /dev/null /etc/issue.d/cockpit.issue

# https://wiki.archlinux.org/title/Podman#Error_when_creating_a_container_with_bridge_network_in_rootless_mode

my-append-new-lines "
owner /run/user/[0-9]*/containers/cni/dnsname/*/dnsmasq.conf r,
owner /run/user/[0-9]*/containers/cni/dnsname/*/addnhosts r,
owner /run/user/[0-9]*/containers/cni/dnsname/*/pidfile rw,
" /etc/apparmor.d/local/usr.sbin.dnsmasq sudo

### libvirt
$_AUR_HELPER -S --noconfirm --needed virt-manager edk2-ovmf libguestfs
sudo gpasswd -a $USER kvm
sudo gpasswd -a $USER libvirt
sudo systemctl enable --now libvirtd.socket

## Virtual Machine Manager
# Toggle keyboard grabbing with L_Ctrl-L_Super
gsettings set org.virt-manager.virt-manager.console grab-keys '65515,65507'
gsettings set org.virt-manager.virt-manager enable-libguestfs-vm-inspection true
gsettings set org.virt-manager.virt-manager.console auto-redirect false
gsettings set org.virt-manager.virt-manager.console resize-guest 1
gsettings set org.virt-manager.virt-manager.console scaling 2
gsettings set org.virt-manager.virt-manager.stats enable-cpu-poll true
gsettings set org.virt-manager.virt-manager.stats enable-disk-poll true
gsettings set org.virt-manager.virt-manager.stats enable-memory-poll true
gsettings set org.virt-manager.virt-manager.stats enable-net-poll true

sudo virsh net-autostart default
# sudo virsh net-start default

### Kernel Same-page Merging
# https://wiki.archlinux.org/title/QEMU#Enabling_KSM

sudo tee /etc/tmpfiles.d/ksm.conf <<<'w /sys/kernel/mm/ksm/run - - - - 1'

### Virtualbox

# https://wiki.archlinux.org/title/VirtualBox#Accessing_host_USB_devices_in_guest
sudo gpasswd -a $USER vboxusers
