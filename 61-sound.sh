#!/usr/bin/env bash
source $(dirname $0)/env.sh

sudo gpasswd -a $USER realtime

# if [[ ! -f ~/.config/wireplumber/main.lua.d/50-alsa-config.lua ]]; then
# 	mkdir -p ~/.config/wireplumber/main.lua.d/
# 	cp /usr/share/wireplumber/main.lua.d/50-alsa-config.lua \
# 		~/.config/wireplumber/main.lua.d/50-alsa-config.lua

# 	sed -i 's/--\["api.alsa.soft-mixer"\].*/["api.alsa.soft-mixer"] = true,/g' \
# 		~/.config/wireplumber/main.lua.d/50-alsa-config.lua

# fi

$_AUR_HELPER -Syu --noconfirm --needed easyeffects

gsettings set com.github.wwmm.easyeffects.streaminputs plugins "['rnnoise']"
gsettings set com.github.wwmm.easyeffects.streamoutputs plugins "['autogain']"
