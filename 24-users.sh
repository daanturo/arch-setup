#!/usr/bin/env bash
source $(dirname $0)/env.sh

### Systemd-homed
sudo systemctl enable --now systemd-homed

echo -n "New username: "
read user

# Create a systemd-homed user

# https://github.com/systemd/systemd/issues/20960 trying to mount a dirty LUKS loopback with homed uses an unexpected amount of disk space

#  distrobox requires that nosuid is unset for sudo
sudo homectl create $user \
    --storage=luks \
    --luks-discard=true \
    --auto-resize-mode=off \
    --luks-extra-mount-options='compress-force=zstd' \
    --nosuid=false \
    -G input,kvm,libvirt,nix-users,realtime,gamemode \
    --disk-size=64G
# --shell=$(which zsh)
