#!/usr/bin/env bash
source $(dirname $0)/env.sh

### Guest user

if ! id guest &>/dev/null; then
    useradd -m guest
    passwd guest
fi

### Boot loader

pacman -S --noconfirm --needed efibootmgr os-prober curl

#### Grub

pacman -S --noconfirm --needed grub

# Note: add "--removable" when undetected
grub-install --efi-directory=$_ESP_MOUNT_POINT --bootloader-id=GRUB --removable

if ! grep --ignore-case --regexp="^Boot0000\*.*\bGRUB\b" <<<"$(efibootmgr)"; then
    # Entry may not be created, or deleted by Windows
    efibootmgr --create --loader "\EFI\GRUB\grubx64.efi" --unicode --disk ${_ESP} --part ${_ESP_PART_NUM} --label "GRUB Boot Loader"
fi

# https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Hide_GRUB_unless_the_Shift_key_is_held_down
if [[ ! -f /etc/grub.d/31_hold_shift ]]; then
    curl 'https://gist.githubusercontent.com/anonymous/8eb2019db2e278ba99be/raw/257f15100fd46aeeb8e33a7629b209d0a14b9975/gistfile1.sh' \
        -o /etc/grub.d/31_hold_shift
    chmod a+x /etc/grub.d/31_hold_shift
    grub-mkconfig -o /boot/grub/grub.cfg
fi

pacman -S --noconfirm --needed grub-btrfs
systemctl enable --now grub-btrfs.path

#### Refind
# # Hide the bootloader unless a key is pressed
sed -i "s/^timeout .*$/timeout -1/g" $_ESP_MOUNT_POINT/EFI/refind/refind.conf
# Don't combine multiple kernels, since the default to boot is the one with the latest timestamp
sed -i -E "s/^[# ]*fold_linux_kernels.*$/fold_linux_kernels false/g" $_ESP_MOUNT_POINT/EFI/refind/refind.conf
sed -i -E "s/^[# ]*extra_kernel_version_strings.*$/extra_kernel_version_strings linux-hardened,linux-zen,linux-lts,linux/g" $_ESP_MOUNT_POINT/EFI/refind/refind.conf
