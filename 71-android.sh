#!/usr/bin/env bash
source $(dirname $0)/env.sh

$_AUR_HELPER -Syu --noconfirm --needed android-udev android-tools

# NOTE: install locally is still a bit more efficient since the AUR build
# directory will take a sizable disk space

### Install locally

# I wish we can just install cmdline-tools globally, but the `sdkmanager`
# executable must reside under a user-writable ANDROID_SDK_ROOT so it will be
# able to operate normally https://stackoverflow.com/a/61176718/8051562

mkdir -p "$ANDROID_SDK_ROOT"

# Install globally, then copy locally
if [[ ! -d "$ANDROID_SDK_ROOT/cmdline-tools" ]]; then
    $_AUR_HELPER -S --noconfirm --needed android-sdk-cmdline-tools-latest
    cp -r /opt/android-sdk/cmdline-tools "$ANDROID_SDK_ROOT"
    $_AUR_HELPER -Rus --noconfirm android-sdk-cmdline-tools-latest
fi

# sdkmanager --sdk_root="$ANDROID_SDK_ROOT"
sdkmanager "platform-tools" "emulator"

latest_platform=$(sdkmanager --list | grep -iE 'platforms;android-[0-9]' |
    sort --version-sort | tail -n1 | pcregrep -o1 ' *([^ ]+).*')

latest_build_tools=$(sdkmanager --list | grep -iE 'build-tools;' |
    sort --version-sort | tail -n1 | pcregrep -o1 ' *([^ ]+).*')

sdkmanager "$latest_build_tools" "$latest_platform"

# Too heavy!
# https://www.nextpit.com/forum/781854/confused-about-the-available-system-images-in-sdk-manager#3226709
latest_system_image=$(sdkmanager --list | grep -iE 'system-images;android-[0-9]+;google_apis;x86_64' |
    sort --version-sort | tail -n1 | pcregrep -o1 ' *([^ ]+).*')

### Install globally

# $_AUR_HELPER -S --noconfirm --needed \
#     android-sdk-cmdline-tools-latest \
#     android-sdk-platform-tools \
#     android-platform
# # android-sdk-build-tools android-emulator android-google-apis-x86-64-system-image

# # https://wiki.archlinux.org/title/Android#Making_/opt/android-sdk_group-writeable
# sudo groupadd android-sdk
## sudo gpasswd -a $USER android-sdk
# sudo setfacl -R -m g:android-sdk:rwx /opt/android-sdk
# sudo setfacl -d -m g:android-sdk:rwX /opt/android-sdk
