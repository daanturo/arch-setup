#!/usr/bin/env bash
source $(dirname $0)/env.sh

### Auto-unlock encrypted root with TPM

command -v tpm2 2>&1 || /dev/null sudo pacman -Syu --noconfirm --needed tpm2-tools

# https://wiki.archlinux.org/title/Trusted_Platform_Module#Data-at-rest_encryption_with_LUKS

# Remove enrolled keys before (re-)enrolling
sudo systemd-cryptenroll $_LUKS_PARTITION --wipe-slot=tpm2

# The differred PCRs between a normal session and a live medium, as observed,
# are: 1, 4, 5.

# 0,2,3,5,6,7 are recommended by
# https://wiki.gentoo.org/wiki/Trusted_Platform_Module#Adding_a_TPM_LUKS_key

# Don't want PCR 7 when Secure Boot is disabled, as it may get turned on later

# sudo systemd-cryptenroll --tpm2-device=auto --tpm2-pcrs=0+2+3+5+6+7 $_LUKS_PARTITION
sudo systemd-cryptenroll --tpm2-device=auto --tpm2-pcrs=0+2+3+5+6 $_LUKS_PARTITION

# https://wiki.archlinux.org/title/Dm-crypt/Specialties#Discard/TRIM_support_for_solid_state_drives_(SSD)
my-append-new-lines "$_LUKS_MAPPER_NAME	$_LUKS_PARTITION	-	tpm2-device=auto,discard" /etc/crypttab sudo
my-append-new-lines "$_LUKS_MAPPER_NAME	$_LUKS_PARTITION	-	tpm2-device=auto,discard" /etc/crypttab.initramfs sudo

sudo mkinitcpio -P
