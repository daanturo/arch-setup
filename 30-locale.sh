#!/usr/bin/env bash

source $(dirname $0)/env.sh

# NOTE: check if glibc is installed by Nix package manager, it may mess up the
# system's locales

my-uncomment-conf "en_US.UTF" /etc/locale.gen sudo
my-uncomment-conf "en_GB.UTF" /etc/locale.gen sudo
# ISO 8601 date format https://wiki.archlinux.org/title/Locale#LC_TIME:_date_and_time_format
my-uncomment-conf "en_DK.UTF" /etc/locale.gen sudo
my-uncomment-conf "vi_VN UTF-8" /etc/locale.gen sudo
sudo locale-gen

# Saner formats than US
sudo localectl set-locale LANG=en_GB.UTF-8
sudo localectl set-locale LANGUAGE=en_GB

sudo localectl set-locale LC_TIME=en_DK.UTF-8

# sudo localectl set-locale LC_TIME=C.UTF-8
