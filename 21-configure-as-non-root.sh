#!/usr/bin/env bash
source $(dirname $0)/env.sh

### AUR helper

# Only sync & update once
sudo pacman -Syu --noconfirm --needed base-devel
if ! command -v $_AUR_HELPER &>/dev/null; then
    git clone 'https://aur.archlinux.org/$_AUR_HELPER-bin.git' /tmp/$_AUR_HELPER-bin
    cd /tmp/$_AUR_HELPER-bin
    makepkg -si --noconfirm
fi

### Time zone

sudo pacman -S --noconfirm --needed uv

_time_zone=$(uvx tzupdate -p)

sudo pacman -S --noconfirm --needed jq
# _time_zone=$(curl -s https://ipinfo.io/json | jq -r '.timezone')

sudo ln -sf /usr/share/zoneinfo/${_time_zone} /etc/localtime
sudo timedatectl set-timezone ${_time_zone}

sudo hwclock --systohc

### Micro code

[[ $_CPU_VENDOR =~ 'Intel' ]] && sudo pacman -S --noconfirm --needed intel-ucode
[[ $_CPU_VENDOR =~ 'AMD' ]] && sudo pacman -S --noconfirm --needed amd-ucode

### Desktop environment

# Avoid countless language-specific fonts in noto-fonts
yes y | $_AUR_HELPER -S --needed noto-fonts-lite

# KDE Plasma
sudo pacman -S --noconfirm --needed plasma-meta konsole
sudo systemctl enable --now sddm

# # SDDM background doesn't/can't change
# sudo ln -sf /usr/share/wallpapers/Next/contents/images/1920x1080.png /usr/share/sddm/themes/breeze/1920x1080.png

sudo pacman -S --noconfirm --needed fish

### Disks

sudo systemctl enable --now fstrim.timer

### Arch Linux-specifics
# Arch Linux
sudo pacman -S --noconfirm --needed pkgstats expac pacman-contrib
$_AUR_HELPER -S --noconfirm --needed reflector downgrade arch-install-scripts

sudo sed -i 's/^--latest [0-9]*$/--latest 7/' /etc/xdg/reflector/reflector.conf
sudo sed -i 's/^--sort [a-z]*$/--sort rate/' /etc/xdg/reflector/reflector.conf
sudo systemctl enable --now reflector.timer
sudo systemctl enable --now paccache.timer

sudo sed -i -E "s/^[# ]+Color/Color/" /etc/pacman.conf
sudo sed -i -E "s/^[# ]+VerbosePkgLists/VerbosePkgLists/" /etc/pacman.conf
sudo sed -i -E "s/^[# ]+ParallelDownloads.*/ParallelDownloads = 128/" /etc/pacman.conf

grep -qE '^\[multilib\]' /etc/pacman.conf ||
    sudo perl -0777 -i -pe 's/^[# ]*(\[multilib\])\n[# ]*(.*)/\1\n\2/m' /etc/pacman.conf

# ILoveCandy must belong to [options]
grep -qE '^ILoveCandy' /etc/pacman.conf ||
    sudo perl -0777 -i -pe 's/^(\[options\])/\1\nILoveCandy/m' /etc/pacman.conf

### Memory

# out-of-memory (OOM)
sudo systemctl enable --now systemd-oomd

### Swap on SSD
sudo tee /etc/sysctl.d/99-swappiness.conf <<<'vm.swappiness=1'
