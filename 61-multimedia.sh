#!/usr/bin/env bash
source $(dirname $0)/env.sh

$_AUR_HELPER -Syu --noconfirm --needed \
    inkscape \
    kdenlive mediainfo \
    krita \
    haruna
# gimp gimp-plugin-resynthesizer-git \

# https://github.com/tesseract-ocr/tesseract OCR
$_AUR_HELPER -S --noconfirm --needed tesseract gimagereader-qt
$_AUR_HELPER -S --noconfirm --needed tesseract-data-eng # English
$_AUR_HELPER -S --noconfirm --needed tesseract-data-ell # modern Greek
$_AUR_HELPER -S --noconfirm --needed tesseract-data-vie # Vietnamese
