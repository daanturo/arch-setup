#!/usr/bin/env bash
source $(dirname $0)/env.sh

user=$1
if [[ $user = "" ]]; then
    echo -n "Username: "
    read user
fi

hashed_password=$(grub-mkpasswd-pbkdf2 | tee /dev/tty | grep -o -E 'grub\.pbkdf2.*')

echo "
/etc/grub.d/40_custom
"

sudo tee -a /etc/grub.d/40_custom <<<"
set superusers='$user'
password_pbkdf2 $user $hashed_password
"

sudo grub-mkconfig -o /boot/grub/grub.cfg
