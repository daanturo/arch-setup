#!/usr/bin/env bash

source $(dirname $0)/env.sh

sudo systemctl enable --now btrfs-scrub@-.timer # Scrub btrfs root

sudo systemctl enable --now my-btrfs-balance@-.timer # Periodic balancing to prevent full disk
