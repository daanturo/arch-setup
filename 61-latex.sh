#!/usr/bin/env bash
source $(dirname $0)/env.sh

$_AUR_HELPER -Syu --noconfirm --needed texlive-meta texlive-langother texlive-binextra
$_AUR_HELPER -Syu --noconfirm --needed biber
