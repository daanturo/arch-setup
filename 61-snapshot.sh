#!/usr/bin/env bash
source $(dirname $0)/env.sh

$_AUR_HELPER -Syu --noconfirm

$_AUR_HELPER -S --noconfirm --needed snapper

sudo systemctl enable --now snapper-cleanup.timer
