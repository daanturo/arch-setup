#!/usr/bin/env bash
source $(dirname $0)/env.sh

### Emacs
$_AUR_HELPER -S --noconfirm --needed \
    cmake libvterm \
    tree-sitter
