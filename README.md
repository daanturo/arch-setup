# Run instruction

Run scripts in the order specified by their numeric prefix.

Notes:

- `env.sh` contains customizable variables (to some extends)

- \<10: Especially `*.bash` files - run in a normal live installation environment (or equivalent) 
- \>=10: Run inside chroot or the installed system
- \>=50: Non-essential settings, should be ran after installation is finished

- Remember to change/set root password!

# Evaluate bootloader choices


|                                                                       | GRUB   | Systemd-boot | rEFInd | EFISTUB |
|-----------------------------------------------------------------------|--------|--------------|--------|---------|
| menu hidden (0s wait time) unless pressing                            | ✔ [4]  | ✅           | ✅     | ?       |
| password-protected kernel parameters editor [0]                       | ✅     | ❗ [3]       | ❌     | ?       |
| multiple entries: (normal, minimal, fallback)×(@kernels)              | ✅     | ❌ [2]       | ✅     | ?       |
| multi-line kernel parameters in config files (comments + readability) | ✅     | ❌ [2]       | ❌ [2] | ?       |
| secure boot support                                                   | ❗ [1] | ✔            | ✔      | ?       |
| boot into btrfs snapshots                                             | ✔      | ❌           | ✔      | ?       |

[0] not too important since I rarely use it, just disabling maybe simpler (https://unix.stackexchange.com/questions/34462/why-does-linux-allow-init-bin-bash)

[1] https://bbs.archlinux.org/viewtopic.php?id=277474, https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#shim_with_key_and_GRUB

[2] write generator manually

[3] 3rd party, dead development https://github.com/kitsunyan/systemd-boot-password/issues/1

[4] manual/3rd party script, may not be compatible with hardware
