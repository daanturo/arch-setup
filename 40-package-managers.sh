#!/usr/bin/env bash
source $(dirname $0)/env.sh

sudo systemctl enable --now my-auto-updates.timer

### Flatpak
$_AUR_HELPER -S --noconfirm --needed flatpak
flatpak --user remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# # deny $HOME access by default, such as Chromium,...
# flatpak override --nofilesystem=home --user

### Nix package manager
$_AUR_HELPER -S --noconfirm --needed nix
sudo gpasswd -a $USER nix-users
sudo systemctl enable --now nix-daemon.socket
sudo nix-channel --add https://nixos.org/channels/nixpkgs-unstable
sudo nix-channel --update
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update

# sudo tee -a /etc/nix/nix.conf && sudo pkill nix-daemon <<<"trusted-users = root $USER"

# Cachix https://app.cachix.org/cache/nix-community#pull
sudo nix-env -iA cachix -f https://cachix.org/api/v1/install
sudo cachix use nix-community

# my-append-new-lines "{ allowUnfree = true; }" /etc/nix/nix.conf sudo

# applicable for root, too
my-append-new-lines "NIXPKGS_ALLOW_UNFREE=1" /etc/environment sudo
my-append-new-lines "NIXPKGS_ALLOW_INSECURE=1" /etc/environment sudo

if ! test -e /etc/profile.d/nix.sh; then
    sudo ln -sf /usr/etc/profile.d/nix.sh /etc/profile.d/nix-my-workaround-missing.sh
else
    sudo rm /etc/profile.d/nix-my-workaround-missing.sh
fi

### Guix

$(dirname $0)/41-guix-package-manager.sh

### Other language-specific package managers

$_AUR_HELPER -S --noconfirm --needed pnpm uv
