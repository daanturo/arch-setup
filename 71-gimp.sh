#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

source $(dirname $0)/env.sh

set -x

## * PhotoGIMP

# https://github.com/Diolinux/PhotoGIMP
# A Patch for GIMP 2.10+ for Photoshop Users

tmp_d=$(mktemp -d)

cd "${tmp_d}"

config_d="$HOME/.var/app/org.gimp.GIMP/config/GIMP/"
if test -d ~/.config/GIMP/; then
    config_d=~/.config/GIMP/
fi

mkdir -p "${config_d}"

git clone --depth=1 https://github.com/Diolinux/PhotoGIMP

cp -r PhotoGIMP/.var/app/org.gimp.GIMP/config/GIMP/* "${config_d}"

rm -rf "${tmp_d}"
