#!/usr/bin/env bash
source $(dirname $0)/env.sh

### Record screen
$_AUR_HELPER -S --noconfirm --needed obs-studio \
    obs-cli \
    pass

## Generate GPG key
# gpg --full-gen-key

# pass init ...
# pass insert .../...
