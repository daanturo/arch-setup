#!/usr/bin/env bash
source $(dirname $0)/env.sh

$_AUR_HELPER -Syu --noconfirm

### Packages building

# https://wiki.archlinux.org/title/Ccache
$_AUR_HELPER -S --noconfirm --needed ccache
sudo sed -iE 's/^BUILDENV=\(.*\) !ccache \(.*\)/BUILDENV=\1 ccache \2/g' /etc/makepkg.conf
grep -qr '^MAKEFLAGS=' /etc/makepkg.conf || sudo tee -a /etc/makepkg.conf <<<'MAKEFLAGS="-j$(($(nproc) / 2))"'
sudo sed -iE 's/CFLAGS="-march=x86-64 -mtune=generic/CFLAGS="-march=native/g' /etc/makepkg.conf

### Git LFS

$_AUR_HELPER -S --noconfirm --needed git-lfs
git lfs install

### Task manager
$_AUR_HELPER -S --noconfirm --needed htop

### KDE

# $_AUR_HELPER -S --noconfirm --needed kde-applications-meta

# https://archlinux.org/packages/extra/any/kde-applications-meta
$_AUR_HELPER -S --noconfirm --needed \
    kde-accessibility-meta \
    kde-graphics-meta \
    kde-network-meta \
    kde-system-meta \
    kde-utilities-meta \
    dolphin

# Video previews for dolphin
$_AUR_HELPER -S --noconfirm --needed ffmpegthumbs

# Gwenview optional image formats
$_AUR_HELPER -S --noconfirm --needed kimageformats qt6-imageformats

# Pretty on-demand panel
$_AUR_HELPER -S --noconfirm --needed latte-dock

# Show window title in the panel, Pomodoroid timer, Extended calendar, Control windows from the panel
$_AUR_HELPER -S --noconfirm --needed plasma6-applets-window-title plasma6-applets-fokus
# With GTK_USE_PORTAL=1, GTK applications may not appear with the correct theme
$_AUR_HELPER -S --noconfirm --needed xdg-desktop-portal-gtk

# # GTK applications all wait for xdg-desktop-portal-gtk, which is a service
# # that was failing to start
# systemctl --user enable --now xdg-desktop-portal-my-ensure.service

# Allow a different color scheme for some applications, by setting QT_QPA_PLATFORMTHEME='qt6ct'
$_AUR_HELPER -S --noconfirm --needed qt6ct

### Themes
# systemctl --user enable --now theme-my-auto-set-color-scheme.timer

### Tap for one key, hold for another
$_AUR_HELPER -S --noconfirm --needed interception-dual-function-keys
sudo systemctl enable --now udevmon

### Browser

# # Use Flatpak
# $_AUR_HELPER -S --noconfirm --needed firefox

### Email client

# $_AUR_HELPER -S --noconfirm --needed thunderbird birdtray-git

# Run in background <3
# $_AUR_HELPER -S --noconfirm --needed mailspring
sudo nix-env -iA nixpkgs.mailspring

### Sound

yes y | $_AUR_HELPER -S --needed alsa-utils \
    pipewire wireplumber \
    pipewire-pulse $(expac -S "%o" pipewire | sed 's/\<pipewire-docs\>//')

### firewalld
$_AUR_HELPER -S --noconfirm --needed firewalld
sudo systemctl enable --now firewalld

#### KDE Connect
sudo firewall-cmd --permanent --zone=public --add-service=kdeconnect
sudo firewall-cmd --reload
# sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/tcp
# sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/ud# p

# sudo firewall-cmd --add-service=samba --permanent
# sudo firewall-cmd --reload

### CLI find, grep,...

$_AUR_HELPER -S --noconfirm --needed fd fzf
$_AUR_HELPER -S --noconfirm --needed ripgrep ripgrep-all

# locate's DB consumes disk space, most of the time findutils's find, or fd is enough

# $_AUR_HELPER -S --noconfirm --needed plocate
# sudo updatedb &
# sudo systemctl enable --now plocate-updatedb.timer

### AppArmor
$_AUR_HELPER -S --noconfirm --needed apparmor
# Speed-up AppArmor start by caching profiles
sudo sed -i "s/^#write-cache$/write-cache/g" /etc/apparmor/parser.conf
sudo systemctl enable --now apparmor.service

### SSH

$_AUR_HELPER -S --noconfirm --needed openssh

mkdir --parents --mode=700 ~/.ssh
my-append-new-lines "
AddKeysToAgent yes
" ~/.ssh/config

# GCR-4 takes over ssh-agent and ssh-askpass, https://gitlab.archlinux.org/archlinux/packaging/packages/gcr-4/-/issues/2
# gcr-ssh-agent does not support custom SSH_ASKPASS / SSH_ASKPASS_REQUIRE / BatchMode, https://gitlab.gnome.org/GNOME/gcr/-/issues/117
sudo systemctl --global disable gcr-ssh-agent.service gcr-ssh-agent.socket

### Command line clipboard access
$_AUR_HELPER -S --noconfirm --needed wl-clipboard

### Bluetooth

$_AUR_HELPER -S --noconfirm --needed bluez-utils
bluetoothctl discoverable on
sudo sed -iE 's/^#\? \?DiscoverableTimeout = [0-9]\+$/DiscoverableTimeout = 0/' /etc/bluetooth/main.conf
sudo sed -iE 's/^#\? \?PairableTimeout = [0-9]\+$/PairableTimeout = 0/' /etc/bluetooth/main.conf
# sudo sed -i 's/^#\? \?\(Class = 0x[0-9]*$\)/\1/' /etc/bluetooth/main.conf
sudo systemctl enable --now bluetooth

### Updater

# $_AUR_HELPER -S --noconfirm --needed topgrade
sudo nix-env -iA nixpkgs.topgrade

### Power saving - Battery life

$_AUR_HELPER -S --noconfirm --needed powertop

# # https://gitlab.freedesktop.org/hadess/power-profiles-daemon/-/issues/73
# if [[ $_CPU_VENDOR =~ 'Intel' ]]; then
$_AUR_HELPER -S --noconfirm --needed power-profiles-daemon
sudo systemctl enable --now power-profiles-daemon
# fi

# if [[ $_CPU_VENDOR =~ 'AMD' ]]; then
#     $_AUR_HELPER -s --noconfirm --needed tlp
#     sudo systemctl enable --now tlp
# fi

### Graphics Drivers
(grep -iq "intel" <<<"$_GPU") && $_AUR_HELPER -S --noconfirm --needed vulkan-intel
(grep -iq "amd" <<<"$_GPU") && $_AUR_HELPER -S --noconfirm --needed vulkan-radeon

### Hardware video acceleration
# https://wiki.archlinux.org/title/Hardware_video_acceleration
$_AUR_HELPER -S --noconfirm --needed libva-mesa-driver mesa-vdpau
$_AUR_HELPER -S --noconfirm --needed gstreamer-vaapi

### CLI Password Manager
$_AUR_HELPER -S --noconfirm --needed pass

### Disable COW on Android Emulator images
mkdir -p ~/.android/avd
chattr -R -c ~/.android/avd
chattr -R +C ~/.android/avd

### CLI Trash
$_AUR_HELPER -S --noconfirm --needed trash-cli

### Fonts
# https://wiki.archlinux.org/title/Microsoft_fonts

# decent CJK coverage without showing countless fonts
$_AUR_HELPER -S --noconfirm --needed ttf-droid
# Display emojis (especially KDE Plasma's emoji picker)
$_AUR_HELPER -S --noconfirm --needed noto-fonts-emoji

$_AUR_HELPER -S --noconfirm --needed font-manager

### Dotfiles
$_AUR_HELPER -S --noconfirm --needed stow

### Core utilities' improved alternatives

$_AUR_HELPER -S --noconfirm --needed bat # colorized cat

# https://wiki.archlinux.org/title/Color_output_in_console#source-highlight
$_AUR_HELPER -S --noconfirm --needed source-highlight

# https://wiki.archlinux.org/title/Color_output_in_console#lesspipe
# view various non-plain-text files
$_AUR_HELPER -S --noconfirm --needed lesspipe

### Archiving and compression

# Extract many formats & Convenience tools
$_AUR_HELPER -S --noconfirm --needed unarchiver atool

# 7z compression
$_AUR_HELPER -S --noconfirm --needed 7zip

### GNOME

command -v gdm &>/dev/null && sudo -u gdm dbus-launch gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click 'true'

# gsettings set org.gnome.desktop.datetime automatic-timezone true
# gsettings set org.gnome.desktop.interface text-scaling-factor 1.25
# gsettings set org.gnome.system.location enabled true
gsettings set org.gnome.desktop.calendar show-weekdate true
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false
gsettings set org.gnome.desktop.peripherals.touchpad speed 0.5
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab']"
gsettings set org.gnome.shell.app-switcher current-workspace-only true

# Free C-M(-S)?-(up|down)
gsettings set org.gnome.desktop.wm.keybindings activate-window-menu "['<Alt>F3']" # from M-SPC
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-down "[]"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-up "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "[]"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "[]"

### Rsync

$_AUR_HELPER -S --noconfirm --needed rsync

### iwd

$_AUR_HELPER -S --noconfirm --needed iwd

### systemd-resolved

# # https://wiki.archlinux.org/title/Systemd-resolved#Setting_DNS_servers
# $_AUR_HELPER -S --noconfirm --needed systemd-resolvconf && sudo systemctl enable --now systemd-resolved.service

### Automation

$_AUR_HELPER -S --noconfirm --needed ydotool
systemctl --user enable --now ydotool

### Kernel tools
$_AUR_HELPER -S --noconfirm --needed linux-tools-meta
