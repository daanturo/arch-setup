#!/usr/bin/env bash
source $(dirname $0)/env.sh

set -euo pipefail
IFS=$'\n\t'

set -x

# https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#sbctl

# It's still not enough with GRUB:
# https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#shim_with_key_and_GRUBa
# Since GRUB version 2.06.r261.g2f4430cc0, loading modules in Secure Boot Mode
# via insmod is no longer allowed, as this would violate the expectation to not
# sideload arbitrary code. If the GRUB modules are not embedded in the EFI
# binary, and GRUB tries to sideload/insmod them, GRUB will fail to boot with
# the message: "error: prohibited by secure boot policy"

sudo pacman -S --noconfirm --needed sbctl

#

sudo sbctl status

my-ask-to-run-file "$0"

sudo sbctl create-keys

# sudo sbctl enroll-keys --microsoft --ignore-immutable
sudo sbctl enroll-keys --microsoft

sudo sbctl status

_unsigned_files=$(
    sudo sbctl verify | { pcregrep -o1 '(/boot/(EFI/|grub/|vmlinuz-)[^ ]+) is not signed$' || true; }
)

for file in $_unsigned_files; do
    sudo sbctl sign -s $file
done
