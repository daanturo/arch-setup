#!/usr/bin/env bash

### Variables

_ESP='/dev/nvme0n1p1'
_ESP_PART_NUM='1'

_LUKS_PARTITION='/dev/nvme0n1p2'
# Put swap after to resize it easier
_SWAP_PARTITION='/dev/nvme0n1p3'

_LUKS_MAPPER_NAME='luksdev'

_MNT='/mnt/arch'

_INIT_PKGS='\
    base linux-firmware \
    linux-zen linux-lts \
    bash-completion \
    base-devel \
    sudo \
    fwupd \
    emacs git \
    man-db
'

_CPU_VENDOR=$(lscpu | grep --ignore-case -E 'Vendor.*: ' | awk '{print $3}')
_GPU=$(lspci | grep -i vga)

# 512MB doesn't seem to be comfortable
_ESP_MOUNT_POINT='/boot'

_AUR_HELPER="paru"

### Functions

export PATH="$PATH:$(dirname $0)/bin"

function my-uncomment-conf() {
    $3 sed -i "s/#[[:space:]]\?$1/$1/g" $2
}

function my-ask-to-run-file() {

    command -v bat &>/dev/null && bat "$0" || cat "$0"

    read -p "
Are you sure to run this? (yes): " yes
    if [[ ! $yes == "yes" ]]; then
        exit
    fi

}

# Print commands and their arguments as they are executed.
set -x
