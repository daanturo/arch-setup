#!/usr/bin/env bash
source $(dirname $0)/env.sh

pacstrap $_MNT $_INIT_PKGS

genfstab -t PARTLABEL $_MNT >>$_MNT/etc/fstab

cp -r $(dirname $0)/files/* $_MNT
