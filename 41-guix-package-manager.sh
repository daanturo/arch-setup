#!/usr/bin/env bash
source $(dirname $0)/env.sh

if ! command -v guix &>/dev/null; then

    cd /tmp
    wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
    chmod a+x guix-install.sh
    # sudo GUIX_ALLOW_OVERWRITE=true ./guix-install.sh
    sudo -v
    yes '' | sudo GUIX_ALLOW_OVERWRITE=true ./guix-install.sh

fi

# If using systemd-homed, the profile may not be created
# https://yhetil.org/guix/87v81kke49.fsf@gmail.com/T/#u
guix_user_dir="/var/guix/profiles/per-user/${USER}"
if ! test -e "${guix_user_dir}"; then
    sudo mkdir -p "${guix_user_dir}"
    sudo chown $USER:$USER "${guix_user_dir}"
fi

# https://guix.gnu.org/manual/en/guix.html#Build-Environment-Setup
sudo groupadd --system guixbuild
for i in $(seq -w 1 16); do
    sudo useradd -g guixbuild -G guixbuild \
        -d /var/empty -s $(which nologin) \
        -c "Guix build user $i" --system \
        guixbuilder$i
done

# sudo guix install glibc-locales
# guix pull
guix install glibc-locales

# # the warning still appears in `sudo guix ...` even when setting in
# # /etc/profile.d/
# my-append-new-lines "GUIX_LOCPATH=/var/guix/profiles/per-user/root/guix-profile/lib/locale/" /etc/environment sudo

# Limit the number of used cores in guix-daemon.service ( --cores=n ), sometimes
# it's run in the background automatically
# https://guix.gnu.org/manual/en/html_node/Invoking-guix_002ddaemon.html

cd /etc/systemd/system/
sudo cp guix-daemon.service guix-my-daemon.service

# guix_daemon_cores=$(python -c "import os ; print(max(os.cpu_count() // 4, 1))")
guix_daemon_cores=2

sudo sed --in-place -E "s:^(ExecStart=/.*/guix-daemon ):\1 --cores=${guix_daemon_cores} :g" guix-my-daemon.service

sudo systemctl disable --now guix-daemon.service
sudo systemctl enable --now guix-my-daemon.service
