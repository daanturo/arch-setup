export PATH="$PATH:/opt/rocm/bin"

export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale/:$GUIX_LOCPATH:/var/guix/profiles/per-user/root/guix-profile/lib/locale/"

# # to check if this file is loaded
# export ZZZZZZZ_MY_GLOBAL_PROFILE="loaded!"

# see also: BASH_ENV
# https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html

# Make binaries installed by root, nix available globally https://bbs.archlinux.org/viewtopic.php?id=303567
export PATH="${PATH}:/nix/var/nix/profiles/default/bin"
