#!/usr/bin/env bash
source $(dirname $0)/env.sh

### ALHP

# https://somegit.dev/ALHP/ALHP.GO

# gpg --recv-key 48998B4039BED1CA

# pacman-my-aur alhp-keyring
# pacman-my-aur alhp-mirrorlist

# Why not ALHP?: https://somegit.dev/ALHP/ALHP.GO/issues/223#issuecomment-3027
# (more chance of breaking Arch)

### CachyOS

# https://wiki.cachyos.org/en/home/Repo

# cd /tmp/

# wget https://mirror.cachyos.org/cachyos-repo.tar.xz
# tar -xavf cachyos-repo.tar.xz && cd cachyos-repo
# sudo ./cachyos-repo.sh

### Chaotic-AUR

# https://aur.chaotic.cx/

sudo pacman-key --init

sudo pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key 3056513887B78AEB
sudo pacman -U --needed 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
sudo pacman -U --needed 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

# my-append-new-lines "
# [chaotic-aur]
# Include = /etc/pacman.d/chaotic-mirrorlist
# " /etc/pacman.conf sudo
