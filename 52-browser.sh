#!/usr/bin/env bash
source $(dirname $0)/env.sh

# Using Flatpak, Firefox can still operate after updating, thus minimize
# downtime, especially when critical. Also for unattended (important) browser
# updates.
sudo flatpak install --noninteractive flathub org.mozilla.firefox

# https://discourse.flathub.org/t/how-to-enable-video-hardware-acceleration-on-flatpak-firefox/3125
# https://www.reddit.com/r/linux/comments/1j6rhpx/upcoming_freedesktop_2308_runtime_release_will/
# https://www.reddit.com/r/linux/comments/1j6rhpx/comment/mgtzeas/
runtime_version=$(flatpak info org.mozilla.firefox --show-runtime | pcregrep -o1 "/([^/]*)\$")
sudo flatpak install --noninteractive flathub org.freedesktop.Platform.ffmpeg-full//${runtime_version}
