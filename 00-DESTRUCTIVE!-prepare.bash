#!/usr/bin/env bash
source $(dirname $0)/env.sh

sudo pacman -Sy --noconfirm --needed bat

my-ask-to-run-file "$0"

# NOTE Assume that physical partitioning has been done

setfont sun12x22

### Update the system clock
timedatectl set-ntp true

### swap
swapon $_SWAP_PARTITION

### dm-crypt root

cryptsetup -y -v luksFormat $_LUKS_PARTITION

# https://wiki.archlinux.org/title/Dm-crypt/Specialties#Disable_workqueue_for_increased_solid_state_drive_(SSD)_performance
cryptsetup --perf-no_read_workqueue --perf-no_write_workqueue --persistent \
    open $_LUKS_PARTITION $_LUKS_MAPPER_NAME

mkfs.btrfs --compress zstd -L arch /dev/mapper/$_LUKS_MAPPER_NAME

mount --mkdir /dev/mapper/$_LUKS_MAPPER_NAME $_MNT -o compress=zstd

# EFI
mount --mkdir $_ESP $_MNT/$_ESP_MOUNT_POINT

### Subvolumes

btrfs subvolume create $_MNT/home
btrfs subvolume create $_MNT/var
btrfs subvolume create $_MNT/srv

# grub-btrfs requires /.snapshots to be a mount point
btrfs subvolume create $_MNT/.snapshots
mount --mkdir /dev/mapper/$_LUKS_MAPPER_NAME $_MNT/.snapshots -o compress=zstd,subvol=/.snapshots
