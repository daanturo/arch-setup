#!/usr/bin/env bash
source $(dirname $0)/env.sh

## * core installation

# https://wiki.archlinux.org/title/Waydroid

$_AUR_HELPER -Syu --noconfirm --needed waydroid python-pyclip
# waydroid-image-gapps

# sudo waydroid init -s GAPPS -f
# sudo systemctl enable --now waydroid-container

sudo firewall-cmd --permanent --zone=trusted --change-interface=waydroid0

sudo firewall-cmd --permanent --zone=trusted --add-port=67/udp
sudo firewall-cmd --permanent --zone=trusted --add-port=53/udp

sudo firewall-cmd --permanent --zone=trusted --add-forward

# sudo firewall-cmd --runtime-to-permanent

sudo firewall-cmd --reload

# https://wiki.archlinux.org/title/Waydroid#dnsmasq:_failed_to_open_pidfile_/run/waydroid-lxc/dnsmasq.pid:_Permission_denied
# AppArmor rule

my-append-new-lines "
@{run}/waydroid-lxc/ r,
@{run}/waydroid-lxc/* rw,
" /etc/apparmor.d/local/usr.sbin.dnsmasq sudo

## * waydroid_script

mkdir -p ~/opt/sources/
cd ~/opt/sources/

# git clone https://github.com/casualsnek/waydroid_script
# cd waydroid_script
# $_AUR_HELPER -Syu --noconfirm --needed python-inquirerpy
# sudo python3 main.py install libndk libhoudini
# sudo python3 main.py install smartdock
